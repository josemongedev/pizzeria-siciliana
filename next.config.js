/** @type {import('next').NextConfig} */

const nextConfig = {
  images: {
    domains: ["images.pexels.com"],
  },
  env: {
    apiServerHost: "http://localhost:3000/api",
  },
  // publicRuntimeConfig:{
  //   API_SERVER_HOST:process.env.API_SERVER_HOST,
  // },
  reactStrictMode: true,
};

module.exports = nextConfig;
