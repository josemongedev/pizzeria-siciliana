import { Box, Button, List, Typography } from "@mui/material";
import React from "react";
import CartItem from "./CartItemSideBar";
import NextLink from "next/link";
import { useDispatch, useSelector } from "react-redux";
import { cartSelector } from "../store/selectors/cart.selectors";

interface ICartProps {
  toggleDrawer: () => void;
}

const Cart: React.FC<ICartProps> = ({ toggleDrawer }) => {
  const cart = useSelector(cartSelector);
  return (
    <Box
      m={1}
      sx={{
        width: 250,
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
      }}
      role="presentation"
      onClick={toggleDrawer}
      onKeyDown={toggleDrawer}
    >
      <Typography variant={"h5"} textAlign={"center"}>
        Cart Summary
      </Typography>
      <List sx={{ width: "100%" }}>
        {!cart.items.length
          ? "No items added yet"
          : cart.items.map((item, index) => (
              <CartItem key={index} item={item} />
            ))}
      </List>
      <NextLink href={"/cart"} passHref>
        <Button variant="contained">View cart</Button>
      </NextLink>
    </Box>
  );
};

export default Cart;
