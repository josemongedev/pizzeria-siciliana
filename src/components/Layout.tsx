import React from "react";
import Footer from "./Footer";
import Navbar from "./Navbar";
import Fonts from "./Fonts";

type Props = {};

const Layout: React.FC = ({ children }) => {
  return (
    <>
      <Fonts />
      <main className={"app-main"}>
        <Navbar />
        {children}
        <Footer />
      </main>
    </>
  );
};

export default Layout;
