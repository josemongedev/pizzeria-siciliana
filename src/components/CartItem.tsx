import { ESizeNames } from "constant/product.constants";
import { ICartItem } from "interfaces/cart.interface";
import Image from "next/image";
import React from "react";
import { FaMinus, FaPlus, FaTimes } from "react-icons/fa";
import { useDispatch } from "react-redux";
import { deleteCartItem, updateCartItem } from "store/state/cart.slice";
import styles from "styles/Cart.module.css";

interface CartItemProps {
  item: ICartItem;
}
export const CartItem: React.FC<CartItemProps> = ({ item }) => {
  const dispatch = useDispatch();
  const { prices, extraOptions } = item.product;
  const sizePrice = prices[item.size];
  const extrasPrice = extraOptions.reduce(
    (total, extra) => total + extra.price,
    0
  );
  const itemPrice = (sizePrice + extrasPrice) * item.quantity;

  const handleQtyUpdate = (newQty: number) => {
    dispatch(
      updateCartItem({
        ...item,
        price: newQty * (sizePrice + extrasPrice),
        quantity: newQty,
      })
    );
  };
  const handleDeleteItem = () => {
    dispatch(deleteCartItem({ product: item.product, size: item.size }));
  };
  return (
    <div className={styles["cart-grid-item"]}>
      <div className={styles["cart-image"]}>
        <Image
          src={item.product.img}
          alt={`${item.quantity} x ${item.product.title} size ${
            ESizeNames[item.size]
          }`}
          layout="fill"
          objectFit="contain"
          objectPosition={"center center"}
        />
      </div>
      <div className={styles["cart-details-list"]}>
        <span>Product: {item.product.title}</span>
        <span>
          Extras:{" "}
          {extraOptions.map((opt) => (
            <i key={opt.text}>{opt.text}&nbsp;</i>
          ))}
        </span>
        <span>Size: {ESizeNames[item.size]}</span>
        <span>Price: ${itemPrice}</span>
        <span>Quantity: {item.quantity}</span>
      </div>
      <div className={styles.cartButtonGroup}>
        <button
          onClick={(e) =>
            handleQtyUpdate(item.quantity < 10 ? item.quantity + 1 : 10)
          }
          className={styles.cartItemButton}
        >
          <FaPlus />
        </button>
        <button
          onClick={(e) =>
            handleQtyUpdate(item.quantity > 1 ? item.quantity - 1 : 1)
          }
          className={styles.cartItemButton}
        >
          <FaMinus />
        </button>
        <button onClick={handleDeleteItem} className={styles.cartItemButton}>
          <FaTimes />
        </button>
      </div>
    </div>
  );
};
