import Image from "next/image";
import React, { useState } from "react";
import { MdReadMore } from "react-icons/md";
import { FaChevronUp, FaChevronDown } from "react-icons/fa";
import styles from "styles/Slider.module.css";
import ProductDetails from "./ProductDetails";
import NextLink from "next/link";

interface ISlider {
  productsList: IFetchProduct[];
}

const Slider: React.FC<ISlider> = ({ productsList }) => {
  const [offset, setOffset] = useState(0);
  const goUp = () =>
    setOffset(offset === productsList.length - 1 ? 0 : offset + 1);
  const goDown = () =>
    setOffset(offset === 0 ? productsList.length - 1 : offset - 1);
  return (
    <>
      <div className={styles.sliderLeft}>
        {productsList.length && (
          <ProductDetails product={productsList[offset]} />
        )}
        <div className={styles.sliderControls}>
          <button
            onClick={goDown}
            className={styles.sliderButton}
            style={{ transform: "scale(-1, 1)" }}
          >
            <FaChevronUp />
          </button>
          <button onClick={goUp} className={styles.sliderButton}>
            <FaChevronDown />
          </button>
          <NextLink href={`/product/${productsList[offset]._id}`} passHref>
            <button className={styles.sliderButton}>
              <MdReadMore />
            </button>
          </NextLink>
        </div>
      </div>
      <div className={styles.sliderRight}>
        <div className={styles.sliderList} style={{ "--idx": offset }}>
          {productsList.map((product: IFetchProduct) => (
            <div
              key={product.idx}
              className={styles.sliderImage}
              data-idx={Math.abs(offset - (product.idx as number))}
            >
              <Image layout="fill" src={product.img} alt="" />
            </div>
          ))}
        </div>
      </div>
    </>
  );
};

export default Slider;
