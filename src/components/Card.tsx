import React from "react";
import Image from "next/image";
import styles from "styles/Card.module.css";

const ProductCard: React.FC<{ featProductImg: string }> = ({
  featProductImg,
  children,
}) => {
  return (
    <div className={styles["hero-card"]}>
      <div className={styles["hero-card-text"]}>{children}</div>
      <a href="#menu">
        <div className={styles["hero-feat-image"]}>
          <Image
            objectPosition={"bottom"}
            objectFit="contain"
            src={featProductImg}
            alt="Featured product"
            layout="fill"
          />
        </div>
      </a>
    </div>
  );
};
export default ProductCard;
