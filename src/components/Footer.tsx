import React from "react";
import styles from "styles/Footer.module.css";
import Image from "next/image";
import mockRestaurants from "constant/locations.json";

const footerImgLeft =
  "https://images.pexels.com/photos/774455/pexels-photo-774455.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";

const footerImgRight =
  "https://images.pexels.com/photos/2878742/pexels-photo-2878742.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";
type Props = {};

const Footer = (props: Props) => {
  return (
    <div className={styles["footer"]}>
      <div className={styles["footer-img"]}>
        <Image
          src={footerImgLeft}
          alt="Wine"
          layout="fill"
          objectFit="contain"
          objectPosition={"left  top"}
        />
      </div>
      <div className={styles["footer-locations"]}>
        <h2 className={styles["footer-title"]}>Come visit us!</h2>
        <h4 className={styles["footer-subtitle"]}>Working Hours</h4>
        <div className={styles["footer-locations-legend"]}>
          <div className={styles["footer-locations-schedule"]}>
            <div>Monday-Friday:</div>
            <span>8:00 - 20:00</span>
            <div>Weekends:</div>
            <span>13:00 - 24:00</span>
            <div></div>
          </div>
        </div>
        <h4 className={styles["footer-subtitle"]}>Locations</h4>
        <div className={styles["footer-locations-grid"]}>
          {mockRestaurants.map((restaurant) => (
            <div
              className={styles["footer-locations-grid-item"]}
              key={restaurant.phone}
            >
              <span>{restaurant.address}.</span>
              <div>
                <span>
                  {restaurant.city} {restaurant.state},
                </span>

                <span>&nbsp;{restaurant.postal}</span>
              </div>

              <span>{restaurant.phone}</span>
            </div>
          ))}
        </div>
      </div>
      <div className={styles["footer-img"]}>
        <Image
          src={footerImgRight}
          alt="Wine"
          layout="fill"
          objectFit="contain"
          objectPosition={"right top"}
        />
      </div>
    </div>
  );
};

export default Footer;
