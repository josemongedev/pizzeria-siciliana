import React from "react";
import styles from "styles/ProductDetails.module.css";

type Props = {
  product: IFetchProduct;
};

const ProductDetails = ({ product }: Props) => {
  return (
    <div className={styles.productContainer}>
      <h2 className={styles.productTitle}>{product.title}</h2>
      <span className={styles.productDescription}>{product.desc}</span>
      <div className={styles.productPrice}>From ${product.prices[0]}</div>
    </div>
  );
};

export default ProductDetails;
