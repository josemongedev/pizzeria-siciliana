import { Avatar, ListItem, ListItemAvatar, ListItemText } from "@mui/material";
import { ESizeNames } from "constant/product.constants";
import { ICartItem } from "interfaces/cart.interface";
import React from "react";

type Props = {
  item: ICartItem;
};
const CartItem: React.FC<Props> = ({ item: { product, size, quantity } }) => {
  return (
    <ListItem alignItems="center">
      <ListItemAvatar>
        <Avatar src={product.img} alt="Cart item" />
        {/* <Image
          layout="fill"
          objectPosition={"left"}
          objectFit="contain"
          src={img}
          alt=""
        /> */}
      </ListItemAvatar>
      <ListItemText
        primary={product.title}
        secondary={`${quantity} x ${ESizeNames[size]}`}
      />
    </ListItem>
  );
};

export default CartItem;
