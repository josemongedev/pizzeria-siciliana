import cx from "classnames";
import Slider from "components/Slider";
import React from "react";
import homeStyles from "styles/Home.module.css";
import styles from "styles/Menu.module.css";

interface IMenuProps {
  productsList: any[];
}
export const Menu: React.FC<IMenuProps> = ({ productsList }) => {
  return (
    <section
      id="menu"
      className={cx(styles["menuContainer"], homeStyles["page-section"])}
    >
      <article className={styles.menuArticle}>
        <h1 className={styles.menuTitle}>Menu List</h1>
        <div className={styles.menuSlider}>
          <Slider productsList={productsList} />
        </div>
      </article>
    </section>
  );
};
