import Image from "next/image";
import React from "react";
import styles from "styles/Story.module.css";
import homeStyles from "styles/Home.module.css";
import cx from "classnames";

const storyImg =
  "https://images.pexels.com/photos/3343621/pexels-photo-3343621.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";
interface IStoryProps {}
export const Story: React.FC<IStoryProps> = ({}) => {
  return (
    <section className={cx(styles.story, homeStyles["page-section"])}>
      <article className={styles["story-article"]}>
        <div className={styles["story-image"]}>
          <Image
            className={styles["story-image"]}
            objectPosition={"left"}
            src={storyImg}
            alt="Story"
            objectFit="cover"
            layout="fill"
          />
        </div>
        <aside className={styles["story-content"]}>
          <h1 className={styles["story-title"]}>Our story:</h1>
          <p className={styles["story-description"]}>
            After a lifetime working at the local police force as a recognized
            officer, S. Montalbano decided to devote his elderly years to purse
            his real life&apos;s passion: italian food.
          </p>
          <p className={styles["story-description"]}>
            So during 2020/2021 in the middle of a world crisis, Pizzeria
            Siciliana opened doors in our town for the best flavors coming
            directly from Sicily, Italy.
          </p>
          <p className={styles["story-description"]}>
            Family and friends decided to help this adventurous man to achieve
            his goal and the menu has only grown bigger ever since!
          </p>
        </aside>
      </article>
    </section>
  );
};
