import cx from "classnames";
import React from "react";
import styles from "styles/Hero.module.css";
import homeStyles from "styles/Home.module.css";

interface IHeroProps {}

export const Hero: React.FC<IHeroProps> = ({}) => {
  return (
    <section
      className={cx(styles["hero-container"], homeStyles["page-section"])}
    >
      <article className={styles["hero-article-left"]}>
        <div className={styles["hero-content-left"]}>
          <h1 className={styles["hero-title"]}>
            Best quality ingredients and recipes!
          </h1>
          <a href="#menu" className={styles["hero-button"]}>
            See our menu!
          </a>
        </div>
      </article>
      <article className={styles["hero-article-right"]}>
        <div className={styles["hero-content-right"]}>
          {/* <Card featProductImg={featProductImg}>
            <a href="#menu">
              <h2 className={styles["hero-feat-product-legend"]}>
                Today&apos;s flavor:
              </h2>
              <div>
                <span className={styles["hero-feat-product-name"]}>
                  Four cheese &nbsp;
                </span>
                <span>
                  pizza only for &nbsp;
                  <strong className={styles["hero-feat-product-price"]}>
                    8.99!
                  </strong>
                </span>
              </div>
            </a>
          </Card> */}
        </div>
      </article>
    </section>
  );
};
