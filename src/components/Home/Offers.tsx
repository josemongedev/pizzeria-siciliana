import styles from "styles/Offers.module.css";
import homeStyles from "styles/Home.module.css";
import React from "react";
import ProductCard from "components/Card";
import cx from "classnames";

const featProductImg =
  "https://images.pexels.com/photos/10779657/pexels-photo-10779657.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260";

export const Offers: React.FC = ({}) => {
  return (
    <section className={cx(styles.offers, homeStyles["page-section"])}>
      <article className={styles.offersArticle}>
        <h1 className={styles.offersTitle}>Newest offers</h1>
        <div className={styles["offers-grid"]}>
          <div className={styles["offers-grid-item"]}>
            <ProductCard featProductImg={featProductImg}>
              <div>
                <h2 style={{ textTransform: "capitalize" }}>Four cheese</h2>
                <span className={styles["product-offer"]}>
                  <s style={{ marginRight: "5%" }}>$15.99</s>
                  <strong className={styles["offers-offer-price"]}>
                    $8.99
                  </strong>
                </span>
              </div>
            </ProductCard>
          </div>
          <div className={styles["offers-grid-item"]}>
            <ProductCard featProductImg={featProductImg}>
              <div>
                <h2 style={{ textTransform: "capitalize" }}>Four cheese</h2>
                <span className={styles["product-offer"]}>
                  <s style={{ marginRight: "5%" }}>$15.99</s>
                  <strong className={styles["offers-offer-price"]}>
                    $8.99
                  </strong>
                </span>
              </div>
            </ProductCard>
          </div>
          <div className={styles["offers-grid-item"]}>
            <ProductCard featProductImg={featProductImg}>
              <div>
                <h2 style={{ textTransform: "capitalize" }}>Four cheese</h2>
                <span className={styles["product-offer"]}>
                  <s style={{ marginRight: "5%" }}>$15.99</s>
                  <strong className={styles["offers-offer-price"]}>
                    $8.99
                  </strong>
                </span>
              </div>
            </ProductCard>
          </div>
        </div>
      </article>
    </section>
  );
};

// export default Products;
