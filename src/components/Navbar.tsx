import { Drawer } from "@mui/material";
import cx from "classnames";
import useCart from "hooks/useCart";
import NextLink from "next/link";
import React, { useState } from "react";
import { BsCartFill } from "react-icons/bs";
import { FaPizzaSlice } from "react-icons/fa";
import { FiPhone } from "react-icons/fi";
import styles from "styles/Navbar.module.css";
import Cart from "./CartSideBar";

type Props = {};

const Navbar = (props: Props) => {
  const { cart, error, isFetching } = useCart();
  console.log("cart :>> ", cart);

  const cartQty = cart ? cart.items.length : 0;
  const [cartOpenState, setCartOpenState] = useState<boolean>(false);

  const toggleDrawer = () => setCartOpenState(!cartOpenState);

  return (
    <>
      <nav className={styles["navbar-container"]}>
        <span className={styles.subnavleft}>
          <span className={styles.element}>
            <span className={styles.row}>
              <FiPhone />
              &nbsp;
              <strong className={styles.phone}>+1-234-567</strong>
            </span>
          </span>
        </span>
        <span className={styles.subnav}>
          <span className={styles.element}>
            <strong className={styles.pagename}>
              <NextLink href="/" passHref>
                <a>Siciliana</a>
              </NextLink>
            </strong>
          </span>
        </span>
        <span className={styles.subnavright}>
          <span className={styles.element}>
            <NextLink href="/#menu" passHref>
              <a>
                <FaPizzaSlice />
              </a>
            </NextLink>
          </span>
          {cartQty ? (
            <span className={cx(styles.element, styles.cart)}>
              <BsCartFill onClick={toggleDrawer} />
              <div className={styles.cartQty}>{cartQty}</div>
            </span>
          ) : (
            <span className={cx(styles.element, styles.cart)}>
              <BsCartFill onClick={toggleDrawer} />
            </span>
          )}
        </span>
      </nav>
      <Drawer anchor={"right"} open={cartOpenState} onClose={toggleDrawer}>
        <Cart toggleDrawer={toggleDrawer} />
      </Drawer>
    </>
  );
};

export default Navbar;
