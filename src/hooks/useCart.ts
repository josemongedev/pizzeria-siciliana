import { ICart } from "interfaces/cart.interface";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  useCreateCartMutation,
  useGetCartQuery,
} from "store/api/restaurant.api";
import { appSelector } from "store/selectors/app.selectors";
import { loadAppConfig, setCart } from "store/state/app.slice";
import { useState } from "react";

const useCart = () => {
  const [createCart, result] = useCreateCartMutation();
  const dispatch = useDispatch();
  const appConfig = useSelector(appSelector);
  const [cartId, setCartId] = useState<string | null>(appConfig.cart);
  const { data: cart, error, isFetching } = useGetCartQuery(cartId);
  //Step 1. Create empty cart where there is no cart
  useEffect(() => {
    if (!appConfig.loaded) {
      dispatch(loadAppConfig());
      //Creates empty cart
      createCart({
        items: [],
        quantity: 0,
        total: 0,
      });
    } else {
      setCartId(appConfig.cart);
    }
  }, [appConfig, dispatch, createCart]);

  //Step 2. Store Cart ID, after POST request
  useEffect(() => {
    if (result.data && !appConfig.cart) {
      console.log("result.status :>> ", result.status);
      if (!result.error) {
        const { _id } = result?.data as ICart;
        dispatch(setCart({ cart: _id as string }));
        setCartId(_id);
      }
    }
  }, [result, appConfig, dispatch]);

  return { cart: cart as ICart, error, isFetching };
};

export default useCart;
