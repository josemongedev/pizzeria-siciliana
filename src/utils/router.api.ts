import { NextApiRequest, NextApiResponse } from "next";

export enum EHttpApiMethod {
  GET = "get",
  POST = "post",
  PUT = "put",
  DELETE = "delete",
}
// Helper method to wait for a middleware to execute before continuing
// And to throw an error when an error happens in a middleware
function runMiddleware(
  req: NextApiRequest,
  res: NextApiResponse,
  fn: TMiddleware
) {
  return new Promise((resolve, reject) => {
    fn(req, res, (result: any) => {
      if (result instanceof Error) {
        return reject(result);
      }

      return resolve(result);
    });
  });
}

export const router =
  (routeController: any, next?: TMiddleware) =>
  async (req: NextApiRequest, res: NextApiResponse) => {
    const method = <THTTPMethod>(req.method as string).toLowerCase();
    const controllerMethods = Object.keys(routeController) as THTTPMethod[];

    try {
      next && (await runMiddleware(req, res, next));
    } catch (error) {
      return res.status(500).end(`Failed initializing middleware`);
    }

    if (controllerMethods.indexOf(method) > -1) {
      return routeController[method](req, res);
    } else {
      res.setHeader("Allow", ["GET", "POST"]);
      return res.status(405).end(`Method ${method} Not Allowed`);
    }
  };
