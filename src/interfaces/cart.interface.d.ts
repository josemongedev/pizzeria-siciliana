interface ICartItem {
  product: IFetchProduct;
  quantity: number;
  size: number;
  price: number;
}

interface ICart {
  _id: string | null;
  items: ICartItem[];
  quantity: number;
  total: number;
}

export type CartItemModelType = ICartItem & Document;
export type CartModelType = ICart & Document;
