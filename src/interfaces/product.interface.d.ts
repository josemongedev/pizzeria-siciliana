interface IExtra {
  text: string;
  price: number;
}

export interface IProduct {
  title: string;
  desc: string;
  img: string;
  prices: number[];
  extraOptions: IExtra[];
}

export type ProductModelType = IProduct & Document;
