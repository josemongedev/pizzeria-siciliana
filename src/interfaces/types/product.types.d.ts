interface IProduct {
  idx?: number;
  _id?: any;
  img: string;
  title?: string;
  prices: number[];
  desc?: string;
}
