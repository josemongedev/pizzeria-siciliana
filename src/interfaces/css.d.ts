// declare module "react" {
//   interface CSSProperties {
//     [key: `--${string}`]: string | number;
//   }
// }
// My css.d.ts file
import * as CSS from "csstype";

declare module "csstype" {
  interface Properties {
    // Add a missing property
    WebkitRocketLauncher?: string;

    // Add a CSS Custom Property
    "--i"?: number;

    // ...or allow any other property
    [index: string]: any;
  }
}
