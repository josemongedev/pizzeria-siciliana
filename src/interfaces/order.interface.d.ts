export interface IOrder {
  customer: string;
  address: string;
  total: number;
  status: number;
  method: number;
}

export type OrderModelType = IOrder & Document;
