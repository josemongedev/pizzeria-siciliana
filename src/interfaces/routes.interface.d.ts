type TMethodHandler = (
  req: NextApiRequest,
  res: NextApiResponse
) => Promise<void>;

type TCallback = (result: any) => void;

type THTTPMethod = "get" | "post" | "put" | "delete";

interface IController {
  [key: THTTPMethod]: TMethodHandler | undefined;
  post?: TMethodHandler;
  get?: TMethodHandler;
  put?: TMethodHandler;
  delete?: TMethodHandler;
}

type TMiddleware = (
  req: NextApiRequest,
  res: NextApiResponse,
  fn: TCallback
) => Promise<any>;
