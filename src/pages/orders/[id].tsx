import { NextPage } from "next";
import React from "react";
import styles from "styles/Orders.module.css";
import { MdDeliveryDining, MdPayment } from "react-icons/md";
import { GiCook } from "react-icons/gi";
import { BsMailbox } from "react-icons/bs";
import { HiCheckCircle } from "react-icons/hi";

type Props = {};

enum EStage {
  PAYMENT = 1,
  PREPARATION = 2,
  DELIVERY = 3,
  ARRIVAL = 4,
  COMPLETED = 5,
}
const OrdersPage: NextPage = (props: Props) => {
  const currentStage = EStage.PREPARATION;
  const statusStyle = (stage: number) =>
    stage === currentStage
      ? styles.inProgress
      : stage < currentStage
      ? styles.completed
      : styles.pending;

  return (
    <section className={styles.orderContainer}>
      <article className={styles.orderArticle}>
        <div className={styles.orderArticleRow}>
          <table className={styles.orderStatusTable}>
            <tr>
              <th>Order ID</th>
              <th>Customer</th>
              <th>Address</th>
              <th>Total</th>
            </tr>
            <tr>
              <td className={styles.orderId}>2345334234</td>
              <td className={styles.customer}>Jane Doe</td>
              <td className={styles.address}>
                3968 Cityview Drive, Northampton, PA, 18067
              </td>
              <td className={styles.total}>$49.7</td>
            </tr>
          </table>
        </div>
        <div className={styles.orderArticleRow}>
          <h3 className={styles.orderArticleRowTitle}>Order tracking stage:</h3>
          <div className={styles.orderDeliverStatus}>
            <div className={statusStyle(EStage.PAYMENT)}>
              <MdPayment /> Payment
              <HiCheckCircle
                style={{
                  display:
                    EStage.PAYMENT < currentStage ? "inline-block" : "none",
                }}
              />
            </div>
            <div className={statusStyle(EStage.PREPARATION)}>
              <GiCook /> Preparation
              <HiCheckCircle
                style={{
                  display:
                    EStage.PREPARATION < currentStage ? "inline-block" : "none",
                }}
              />
            </div>
            <div className={statusStyle(EStage.DELIVERY)}>
              <MdDeliveryDining /> Delivery
              <HiCheckCircle
                style={{
                  display:
                    EStage.DELIVERY < currentStage ? "inline-block" : "none",
                }}
              />
            </div>
            <div className={statusStyle(EStage.ARRIVAL)}>
              <BsMailbox /> Destination arrived
              <HiCheckCircle
                style={{
                  display:
                    EStage.ARRIVAL < currentStage ? "inline-block" : "none",
                }}
              />
            </div>
          </div>
        </div>
      </article>
    </section>
  );
};

export default OrdersPage;
