import {
  deleteProductByIdController,
  getProductByIdController,
  putProductByIdController,
} from "controllers/products.controller";
import { connectToDatabase } from "middleware/data.middleware";
import { router } from "utils/router.api";

// All requests are done at the respective '/api/products/[id]' endpoint
const productIdController: IController = {
  get: getProductByIdController,

  put: putProductByIdController,

  delete: deleteProductByIdController,
};

const productIdRoute = router(productIdController, connectToDatabase);

export default productIdRoute;
