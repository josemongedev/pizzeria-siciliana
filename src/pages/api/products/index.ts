import { postProductController } from "controllers/products.controller";
import { router } from "utils/router.api";
import { connectToDatabase } from "middleware/data.middleware";
import { getProductsController } from "controllers/products.controller";

// All requests are done at the respective '/api/products' endpoint
const productsController: IController = {
  post: postProductController,

  get: getProductsController,
};

const productsRoute = router(productsController, connectToDatabase);

export default productsRoute;
