import { postCartController } from "controllers/carts.controller";
import { router } from "utils/router.api";
import { connectToDatabase } from "middleware/data.middleware";
import { getCartsController } from "controllers/carts.controller";

// All requests are done at the respective '/api/Carts' endpoint
const cartsController: IController = {
  post: postCartController,

  get: getCartsController,
};

const CartsRoute = router(cartsController, connectToDatabase);

export default CartsRoute;
