import {
  deleteCartByIdController,
  getCartByIdController,
  putCartByIdController,
} from "controllers/carts.controller";
import { connectToDatabase } from "middleware/data.middleware";
import { router } from "utils/router.api";

// All requests are done at the respective '/api/carts/[id]' endpoint
const cartIdController: IController = {
  get: getCartByIdController,

  put: putCartByIdController,

  delete: deleteCartByIdController,
};

const CartIdRoute = router(cartIdController, connectToDatabase);

export default CartIdRoute;
