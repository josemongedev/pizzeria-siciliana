import { connectToDatabase } from "middleware/data.middleware";
import { NextApiRequest, NextApiResponse } from "next";
import { router } from "utils/router.api";

// All requests are done at the respective '/api/products/[id]' endpoint
const helloController: IController = {
  get: async (req: NextApiRequest, res: NextApiResponse) => {
    try {
      return res.status(200).json("hello world");
    } catch (error) {
      return res.status(500).json(error);
    }
  },
};

const helloRoute = router(helloController, connectToDatabase);

export default helloRoute;
