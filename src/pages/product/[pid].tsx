import cx from "classnames";
import { EProductSize } from "constant/product.constants";
import {
  GetServerSideProps,
  InferGetServerSidePropsType,
  NextPage,
} from "next";
import Image from "next/image";
import React, { ChangeEvent, useEffect, useState } from "react";
import { BsFillCartPlusFill } from "react-icons/bs";
import { GiFullPizza } from "react-icons/gi";
import { useDispatch } from "react-redux";
import { addToCart } from "store/state/cart.slice";
import styles from "styles/Product.module.css";
import { getProductById } from "../../services/products.requests";

const ProductPage: NextPage = ({
  product,
  error,
  id,
  message,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  const dispatch = useDispatch();
  const [price, setPrice] = useState(product.prices[EProductSize.SMALL]);
  // Zero Price means the extra is not incorporated
  // into the final price(user didn't check it)
  const [extras, setExtras] = useState<IProductOption[]>(
    product.extraOptions.map((opt: IProductOption) => ({ ...opt, price: 0 }))
  );
  const [size, setSize] = useState<EProductSize>(EProductSize.SMALL);
  const [quantity, setQuantity] = useState(1);

  const handleSize = (sizeIdx: EProductSize) => {
    setSize(sizeIdx);
  };

  const handleExtras = (
    e: ChangeEvent<HTMLInputElement>,
    option: IProductOption
  ) => {
    const { checked } = e.target;
    const unchangedExtras = extras.filter((opt) => opt.text !== option.text);
    setExtras([
      ...unchangedExtras,
      { ...option, price: checked ? option.price : 0 },
    ]);
  };

  const handleCartAdd: React.MouseEventHandler<HTMLButtonElement> = (e) => {
    dispatch(
      addToCart({
        product: {
          ...product,
          extraOptions: extras.filter((extra) => extra.price !== 0),
        },
        price,
        size,
        quantity,
      })
    );
  };

  useEffect(() => {
    const extrasCost = extras.reduce((acc, curr) => (acc += curr.price), 0);
    const finalPrice = (product.prices[size] + extrasCost) * quantity;
    setPrice(finalPrice);
  }, [size, extras, product.prices, quantity]);

  return (
    <>
      {product && (
        <div className={styles.productContainer}>
          <div className={styles.productWrapper}>
            <div className={styles.productLeft}>
              <div className={styles.productImage}>
                <Image
                  objectPosition={"center"}
                  objectFit="contain"
                  src={product.img}
                  alt="Featured product"
                  layout="fill"
                />
              </div>
            </div>
            <div className={styles.productRight}>
              <div className={styles.productInfo}>
                <h1 className={cx(styles.productTitle)}>{product.title}</h1>

                <span
                  className={cx(
                    styles.productNormalText,
                    styles.productDescription
                  )}
                >
                  {product.desc}
                </span>

                {product.extraOptions.length && (
                  <>
                    <h2 className={cx(styles.productSubtitle)}>
                      Extra ingredients:
                    </h2>
                    <div className={cx(styles.productExtrasList)}>
                      {product.extraOptions.map((option: IProductOption) => (
                        <span
                          key={option.text}
                          className={styles.productExtraItem}
                        >
                          <input
                            type="checkbox"
                            id={option.text}
                            name={option.text}
                            value={option.price}
                            onChange={(e) => handleExtras(e, option)}
                          />
                          <label
                            className={styles.productNormalText}
                            htmlFor={option.text}
                          >
                            {option.text}
                          </label>
                        </span>
                      ))}
                    </div>
                  </>
                )}
                <h2 className={cx(styles.productSubtitle)}>Size: </h2>
                <div className={cx(styles.productSizesList)} id="size">
                  <span
                    onClick={() => handleSize(EProductSize.SMALL)}
                    className={styles.productSizeItem}
                  >
                    <GiFullPizza
                      color={size === EProductSize.SMALL ? "yellow" : ""}
                      size={"1.6em"}
                    />
                    <span className={styles.productNormalText}>Small</span>
                  </span>
                  <span
                    onClick={() => handleSize(EProductSize.MEDIUM)}
                    className={styles.productSizeItem}
                  >
                    <GiFullPizza
                      color={size === EProductSize.MEDIUM ? "yellow" : ""}
                      size={"2em"}
                    />
                    <span className={styles.productNormalText}>Medium</span>
                  </span>
                  <span
                    onClick={() => handleSize(EProductSize.LARGE)}
                    className={styles.productSizeItem}
                  >
                    <GiFullPizza
                      color={size === EProductSize.LARGE ? "yellow" : ""}
                      size={"2.4em"}
                    />
                    <span className={styles.productNormalText}>Large</span>
                  </span>
                </div>

                <div className={cx(styles.productQuantity)}>
                  <h2 className={cx(styles.productSubtitle)}>Quantity:</h2>
                  <div className={styles.productAdd}>
                    <input
                      className={cx(
                        styles.productQuantityInput,
                        styles.productNormalText
                      )}
                      type="number"
                      id="quantity"
                      name="quantity"
                      min="1"
                      defaultValue={1}
                      onChange={(e) => setQuantity(parseInt(e.target.value))}
                      max="10"
                    />
                    <button
                      onClick={handleCartAdd}
                      className={cx(
                        styles.productAddCartButton,
                        styles.productNormalText
                      )}
                    >
                      Add to cart&nbsp;
                      <BsFillCartPlusFill />
                    </button>
                  </div>
                  <div className={cx(styles.productPrice)}>
                    <h2 className={cx(styles.productSubtitle)}>Price:</h2>$
                    <span className={styles.productNormalText}>
                      {price.toFixed(2)} (unit price)
                    </span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
};

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  try {
    const res = await getProductById(params?.pid as string);
    return {
      props: {
        product: res.data,
      },
    };
  } catch (error: any) {
    return {
      props: {
        error: true,
        product: null,
        message: error.message,
      },
    };
  }
};

export default ProductPage;
