import cx from "classnames";
import { CartItem } from "components/CartItem";
import { NextPage } from "next";
import NextLink from "next/link";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { cartSelector } from "store/selectors/cart.selectors";
import styles from "styles/Cart.module.css";

type CartProps = {};

const Cart: NextPage = (props: CartProps) => {
  const dispatch = useDispatch();
  const cart = useSelector(cartSelector);
  return (
    <section className={cx(styles.cart)}>
      <article className={styles["cart-article"]}>
        <h1 className={styles["cart-title"]}>Cart</h1>
        <h3 className={styles["cart-subtitle"]}>
          Change the amount or confirm checkout
        </h3>
        <div className={styles["cart-grid"]}>
          {!cart.items.length
            ? "No items added to cart yet"
            : cart.items.map((item) => (
                <>
                  <CartItem
                    key={item.product._id + item.quantity + item.size}
                    item={item}
                  />
                  <hr className={styles.cartSeparator} />
                </>
              ))}
        </div>
        <div className={styles.cartGridCheckout}>
          <h3 className={styles.cartCheckoutItem}>Subtotal:</h3>
          <span className={styles.cartCheckoutItem}>${cart.total} </span>
          <h3 className={styles.cartCheckoutItem}>Total:</h3>
          <span className={styles.cartCheckoutItem}>${cart.total} </span>
          <NextLink href={"/#menu"} passHref>
            <button className={styles.cartButton}>Back to menu</button>
          </NextLink>
          <button className={styles.cartButton}>Confirm order</button>
        </div>
      </article>
    </section>
  );
};

export default Cart;
