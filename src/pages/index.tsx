import { Hero, Menu, Offers, Story } from "components/Home";
import type {
  GetServerSideProps,
  InferGetServerSidePropsType,
  NextPage,
} from "next";
import { getProducts } from "services/products.requests";

const Home: NextPage = ({
  productsList,
  error,
  message,
}: InferGetServerSidePropsType<typeof getServerSideProps>) => {
  return (
    <>
      <Hero />
      <Offers />
      <Menu productsList={productsList} />
      <Story />
    </>
  );
};

export default Home;

export const getServerSideProps: GetServerSideProps = async (context) => {
  try {
    const res = await getProducts();
    return {
      props: {
        productsList: res.data,
      },
    };
  } catch (error: any) {
    return {
      props: {
        error: true,
        productsList: null,
        message: error.message,
      },
    };
  }
};
