import { ProductModelType } from "interfaces/product.interface";
import mongoose from "mongoose";

export const ProductSchema = new mongoose.Schema<ProductModelType>(
  {
    title: {
      type: String,
      required: true,
      maxlength: 60,
    },
    desc: {
      type: String,
      required: true,
      maxlength: 200,
    },
    img: {
      type: String,
      required: true,
      maxlength: 200,
    },
    prices: {
      type: [Number],
      required: true,
    },
    extraOptions: {
      type: [
        {
          text: { type: String, required: true },
          price: { type: Number, required: true },
        },
      ],
    },
  },
  { timestamps: true }
);

// Avoids recreating model again after being created the first time
export default mongoose.models.Product ||
  mongoose.model<ProductModelType>("Product", ProductSchema);
