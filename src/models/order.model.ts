import { OrderModelType } from "interfaces/order.interface";
import mongoose from "mongoose";

const OrderSchema = new mongoose.Schema<OrderModelType>(
  {
    customer: {
      type: String,
      required: true,
      maxlength: 60,
    },
    address: {
      type: String,
      required: true,
      maxlength: 200,
    },
    total: {
      type: Number,
      required: true,
    },
    status: {
      type: Number,
      default: 0,
    },
    method: {
      type: Number,
      required: true,
    },
  },
  { timestamps: true }
);

// Avoids recreating model again after being created the first time
export default mongoose.models.Order ||
  mongoose.model<OrderModelType>("Order", OrderSchema);
