import { CartItemModelType, CartModelType } from "interfaces/cart.interface";
import mongoose from "mongoose";
import { ProductSchema } from "./product.model";

const CartItemSchema = new mongoose.Schema<CartItemModelType>({
  quantity: {
    type: Number,
    required: true,
  },
  size: {
    type: Number,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  product: { type: ProductSchema, required: true },
});

const CartSchema = new mongoose.Schema<CartModelType>({
  items: { type: [CartItemSchema], required: true },
  quantity: {
    type: Number,
    required: true,
  },
  total: {
    type: Number,
    required: true,
  },
});

// Avoids recreating model again after being created the first time
export default mongoose.models.Cart ||
  mongoose.model<CartModelType>("Cart", CartSchema);
