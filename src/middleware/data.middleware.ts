import { NextApiRequest, NextApiResponse } from "next";
import dbConnect from "utils/mongo.connect";

export const connectToDatabase: TMiddleware = async (
  req: NextApiRequest,
  res: NextApiResponse,
  fn: TCallback
) => {
  fn(await dbConnect());
};
