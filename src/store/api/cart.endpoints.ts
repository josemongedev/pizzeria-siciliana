import { ICart } from "interfaces/cart.interface";
import { EHttpApiMethod } from "utils/router.api";
import { createQueryRequest, createMutationRequest } from "./restaurant.api";

export const cartEndpoints = (builder: TRestaurantAPIBuilder) => ({
  getCart: builder.query<IFetchCart, ICart>({
    query: (cartId: string) => createQueryRequest(`/carts/${cartId}`),
    providesTags: [{ type: "Cart", id: "SHOPPING_CART" }],
  }),
  createCart: builder.mutation<ICart, ICart>({
    query: (cart: ICart) =>
      createMutationRequest("/carts", EHttpApiMethod.POST, cart),
    invalidatesTags: [{ type: "Cart", id: "SHOPPING_CART" }],
  }),
  updateCart: builder.mutation<IFetchCart, IUpdateCart>({
    query: ({ cartId, cart }: IUpdateCart) =>
      createMutationRequest(`/carts/${cartId}`, EHttpApiMethod.PUT, cart),
    invalidatesTags: [{ type: "Cart", id: "SHOPPING_CART" }],
  }),
  deleteCart: builder.mutation<void, string>({
    query: (cartId: string) =>
      createMutationRequest(`/carts/${cartId}`, EHttpApiMethod.DELETE),
    invalidatesTags: [{ type: "Cart", id: "SHOPPING_CART" }],
  }),
});
