import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { HYDRATE } from "next-redux-wrapper";
import { EHttpApiMethod } from "utils/router.api";
import { cartEndpoints } from "./cart.endpoints";
import { productsEndpoints } from "./product.endpoints";

const headers = {};

interface IMutationRequestParams {
  url: string;
  method: EHttpApiMethod;
  body?: any;
}
export const createMutationRequest = (
  url: string,
  method: EHttpApiMethod,
  body?: any
): IMutationRequestParams => ({ url, method, body });

export const createQueryRequest = (url: string) => ({ url, headers });

// Requests that use server cookie
export const createAuthQueryRequest = (url: string) => ({
  ...createQueryRequest(url),
  credentials: "include",
});

export const createAuthMutationRequest = (
  url: string,
  method: EHttpApiMethod,
  body?: any
) => ({
  ...createMutationRequest(url, method, body),
  credentials: "include",
});

const baseUrl = "/api";
console.log("baseUrl :>> ", baseUrl);
// API configuration
export const restaurantApi = createApi({
  reducerPath: "restaurantApi",
  extractRehydrationInfo(action, { reducerPath }) {
    if (action.type === HYDRATE) {
      return action.payload[reducerPath];
    }
  },
  tagTypes: ["Product", "Cart"],
  baseQuery: fetchBaseQuery({ baseUrl }),
  endpoints: (builder) => ({
    ...productsEndpoints(builder),
    ...cartEndpoints(builder),
  }),
});

export const {
  useGetProductsQuery,
  useGetProductQuery,
  useCreateProductMutation,
  useUpdateProductMutation,
  useDeleteProductMutation,
  useGetCartQuery,
  useCreateCartMutation,
  useUpdateCartMutation,
  useDeleteCartMutation,
} = restaurantApi;
