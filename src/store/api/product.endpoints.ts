import { EHttpApiMethod } from "utils/router.api";
import { createQueryRequest, createMutationRequest } from "./restaurant.api";

export const productsEndpoints = (builder: TRestaurantAPIBuilder) => ({
  getProducts: builder.query<IFetchProduct[], void>({
    query: () => createQueryRequest("/products"),
    providesTags: [{ type: "Product", id: "PRODUCTS_LIST" }],
  }),
  getProduct: builder.query<IFetchProduct, string>({
    query: (productId: string) => createQueryRequest(`/products/${productId}`),
    providesTags: [{ type: "Product", id: "PRODUCTS_LIST" }],
  }),
  createProduct: builder.mutation<IFetchProduct, IFetchProduct>({
    query: (product: IFetchProduct) =>
      createMutationRequest("/products", EHttpApiMethod.POST, product),
    invalidatesTags: [{ type: "Product", id: "PRODUCTS_LIST" }],
  }),
  updateProduct: builder.mutation<IFetchProduct, IUpdateProduct>({
    query: ({ productId, product }: IUpdateProduct) =>
      createMutationRequest(
        `/products/${productId}`,
        EHttpApiMethod.PUT,
        product
      ),
    invalidatesTags: [{ type: "Product", id: "PRODUCTS_LIST" }],
  }),
  deleteProduct: builder.mutation<void, string>({
    query: (productId: string) =>
      createMutationRequest(`/products/${productId}`, EHttpApiMethod.DELETE),
    invalidatesTags: [{ type: "Product", id: "PRODUCTS_LIST" }],
  }),
});
