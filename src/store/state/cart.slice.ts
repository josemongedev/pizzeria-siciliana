import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { ICart } from "interfaces/cart.interface";

const initialState: ICart = {
  _id: null,
  items: [],
  quantity: 0,
  total: 0,
};

interface ICartPayload {
  product: IFetchProduct;
  price: number;
  size: number;
  quantity: number;
}

type TItemDeleted = Omit<ICartPayload, "price" | "quantity">;

export const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {
    addToCart: (state, action: PayloadAction<ICartPayload>) => {
      const { price, size, quantity, product } = action.payload;
      state.items.push({ product, quantity, size, price });
      state.quantity += 1;
      state.total = state.items.reduce((total, item) => total + item.price, 0);
    },
    updateCartItem: (state, action: PayloadAction<ICartPayload>) => {
      const { price, size, quantity, product } = action.payload;
      state.items = state.items.map((item) => {
        if (item.product._id === product._id && item.size === size) {
          item.quantity = quantity;
          item.price = price;
        }
        return item;
      });
      state.total = state.items.reduce((total, item) => total + item.price, 0);
    },
    deleteCartItem: (state, action: PayloadAction<TItemDeleted>) => {
      const { size, product } = action.payload;
      state.items = state.items.filter((item) => {
        const removedItem =
          item.product._id === product._id && item.size === size;
        if (!removedItem) return item;
      });
      state.quantity -= 1;
      state.total = state.items.reduce((total, item) => total + item.price, 0);
    },
    defaultState: (state) => {
      state = initialState;
    },
  },
});

// Action creators are generated for each case reducer function
export const { addToCart, defaultState, updateCartItem, deleteCartItem } =
  cartSlice.actions;

export default cartSlice.reducer;
