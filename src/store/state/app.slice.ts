import { createSlice, PayloadAction } from "@reduxjs/toolkit";

export interface AppState {
  name: string | null;
  token: string | null;
  cart: string | null;
  loaded: boolean;
}

const initialState: AppState = {
  name: null,
  token: null,
  cart: null,
  loaded: false,
};

export const appSlice = createSlice({
  name: "app",
  initialState,
  reducers: {
    loadAppConfig: (state) => {
      const persisted = JSON.parse(localStorage.getItem("appState") as string);
      Object.assign(state, persisted ? persisted : initialState);
      state.loaded = true;
    },
    setConfig: (
      state,
      action: PayloadAction<{
        name: string;
        token: string;
        cart: string;
      }>
    ) => {
      state.name = action.payload.name;
      state.token = action.payload.token;
      state.cart = action.payload.cart;
      state.loaded = true;
      localStorage.setItem("appState", JSON.stringify(action.payload));
    },
    setCart: (state, action: PayloadAction<{ cart: string }>) => {
      state.cart = action.payload.cart;
      state.loaded = true;
      localStorage.setItem("appState", JSON.stringify(state));
    },
    defaultState: (state) => {
      Object.assign(state, initialState);
      localStorage.removeItem("appState");
    },
  },
});

// Action creators are generated for each case reducer function
export const { loadAppConfig, setConfig, setCart, defaultState } =
  appSlice.actions;

export default appSlice.reducer;
