import { configureStore } from "@reduxjs/toolkit";
import cartReducer from "store/state/cart.slice";
import appReducer from "store/state/app.slice";
import { setupListeners } from "@reduxjs/toolkit/query";
import { restaurantApi } from "./api/restaurant.api";

export const store = configureStore({
  reducer: {
    app: appReducer,
    cart: cartReducer,
    [restaurantApi.reducerPath]: restaurantApi.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(restaurantApi.middleware),
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type: {posts: PostsState, comments: CommentsState, users: UsersState}
export type AppDispatch = typeof store.dispatch;
setupListeners(store.dispatch);
