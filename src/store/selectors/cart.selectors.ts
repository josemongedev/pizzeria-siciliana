import { RootState } from "../index";

export const cartSelector = (state: RootState) => state.cart;
export const cartQtySelector = (state: RootState) => state.cart.quantity;
