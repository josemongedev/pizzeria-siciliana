import { RootState } from "../index";

export const appSelector = (state: RootState) => state.app;
export const cartAppSelector = (state: RootState) => state.app.cart;
