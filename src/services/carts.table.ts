import CartModel from "models/cart.model";
import { ICart } from "interfaces/cart.interface";

export const createCart = async (Cart: any) => await CartModel.create(Cart);

export const readCarts = async () => await CartModel.find();

export const readCartById = async (id: string) => await CartModel.findById(id);

export const updateCart = async (id: string, body: Partial<ICart>) =>
  await CartModel.findByIdAndUpdate(id, { $set: body }, { new: true });

export const deleteCart = async (id: any) =>
  await CartModel.findByIdAndDelete(id);
