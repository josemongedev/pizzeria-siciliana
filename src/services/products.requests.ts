import axios from "axios";

export const getProducts = async () =>
  await axios.get<IFetchProduct[]>(`${process.env.API_SERVER_HOST}/products`);

export const getProductById = async (id: string) =>
  await axios.get<IFetchProduct>(
    `${process.env.API_SERVER_HOST}/products/${id}`
  );
