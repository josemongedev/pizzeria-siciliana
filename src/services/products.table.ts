import ProductModel from "models/product.model";

export const createProduct = async (product: any) =>
  await ProductModel.create(product);

export const readProducts = async () => await ProductModel.find();

export const readProductById = async (id: string) =>
  await ProductModel.findById(id);

export const updateProduct = async (product: any) =>
  await ProductModel.create(product);

export const deleteProduct = async (product: any) =>
  await ProductModel.create(product);
