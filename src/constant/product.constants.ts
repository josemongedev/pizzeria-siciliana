export enum EProductSize {
  SMALL = 0,
  MEDIUM = 1,
  LARGE = 2,
}

export const ESizeNames = Object.values(EProductSize);
