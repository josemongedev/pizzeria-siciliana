import { NextApiRequest, NextApiResponse } from "next";
import {
  createCart,
  deleteCart,
  readCartById,
  readCarts,
  updateCart,
} from "services/carts.table";

export const getCartsController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  try {
    const carts = await readCarts();
    return res.status(200).json(carts);
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const getCartByIdController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const id = req.query.id as string;
  try {
    const cart = await readCartById(id);
    return res.status(200).json(cart);
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const postCartController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  try {
    const cart = await createCart(req.body);
    return res.status(200).json(cart);
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const putCartByIdController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const {
    query: { id },
  } = req;
  try {
    const cart = await updateCart(id as string, req.body);
    return res.status(200).json(cart);
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const deleteCartByIdController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const {
    query: { id },
  } = req;
  try {
    const result = await deleteCart(id);
    const msg = result
      ? `Cart with ID ${id} deleted`
      : "No cart with that ID found.";
    const code = result ? 200 : 400;
    return res.status(code).json(msg);
  } catch (error) {
    return res.status(500).json(error);
  }
};
