import { NextApiRequest, NextApiResponse } from "next";
import { createProduct, updateProduct } from "services/products.table";
import {
  readProducts,
  readProductById,
  deleteProduct,
} from "../services/products.table";

export const getProductsController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  try {
    const products = await readProducts();
    return res.status(200).json(products);
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const getProductByIdController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const id = req.query.id as string;
  try {
    const product = await readProductById(id);
    return res.status(200).json(product);
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const postProductController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  try {
    const product = await createProduct(req.body);
    return res.status(200).json(product);
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const putProductByIdController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const {
    query: { id },
  } = req;
  try {
    const product = await updateProduct(req.body);
    return res.status(200).json(product);
  } catch (error) {
    return res.status(500).json(error);
  }
};

export const deleteProductByIdController = async (
  req: NextApiRequest,
  res: NextApiResponse
) => {
  const {
    query: { id },
  } = req;
  try {
    const result = await deleteProduct(id);
    const msg = result
      ? `Product with ID ${id} deleted`
      : "No product with that ID found";
    const code = result ? 200 : 400;
    return res.status(code).json(msg);
  } catch (error) {
    return res.status(500).json(error);
  }
};
